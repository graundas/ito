<?php
declare(strict_types=1);
namespace App\Controller;

use App\Entity\User;
use App\Entity\UserLanguage;
use App\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin_index")
     */
    public function index(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findBy(['disabled' => false]);

        return $this->render('admin/index.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/create-user", name="admin_create_user")
     */
    public function createUser(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $user->addUserLanguage(new UserLanguage());
        return $this->editUser($user, $request, $encoder);
    }

    /**
     * @Route("/edit-user/{id}", name="admin_edit_user")
     */
    public function editUser(User $user, Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($user->getUserLanguages() as $userLanguage) {
                $userLanguage->setUser($user);
            }

            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/create_user.html.twig', [
            'form' => $form->createView(),
            'editMode' => !!$user->getId(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/delete-user/{id}", name="admin_delete_user")
     */
    public function deleteUser(User $user): Response
    {
        $user->setDisabled(true);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute('admin_index');
    }
}
