<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    public const ENTITY_TYPES = ['private', 'legal'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 8)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     */
    private $fullName;

    /**
     * @var string
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=User::ENTITY_TYPES, message="Choose a valid entity type.")
     */
    private $entityType = 'private';

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $personalCode;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyCode;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $disabled = false;

    /**
     * @ORM\OneToMany(targetEntity=UserLanguage::class, mappedBy="user", orphanRemoval=true, cascade={"persist", "remove"})
     *
     * @Assert\Valid()
     * @Assert\Count(
     *     min=1,
     *     minMessage="You should add at least one language."
     * )
     */
    private $userLanguages;

    public function __construct()
    {
        $this->userLanguages = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return User
     */
    public function setFullName(?string $fullName): User
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->entityType;
    }

    /**
     * @param string $entityType
     * @return User
     */
    public function setEntityType(?string $entityType): User
    {
        $this->entityType = $entityType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPersonalCode(): ?string
    {
        return $this->personalCode;
    }

    /**
     * @param string $personalCode
     * @return User
     */
    public function setPersonalCode(?string $personalCode): User
    {
        $this->personalCode = $personalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     * @return User
     */
    public function setCompanyName(?string $companyName): User
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyCode(): ?string
    {
        return $this->companyCode;
    }

    /**
     * @param string $companyCode
     * @return User
     */
    public function setCompanyCode(?string $companyCode): User
    {
        $this->companyCode = $companyCode;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     * @return User
     */
    public function setDisabled(bool $disabled): User
    {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|UserLanguage[]
     */
    public function getUserLanguages(): Collection
    {
        return $this->userLanguages;
    }

    public function addUserLanguage(UserLanguage $userLanguage): self
    {
        if (!$this->userLanguages->contains($userLanguage)) {
            $this->userLanguages[] = $userLanguage;
            $userLanguage->setUser($this);
        }

        return $this;
    }

    public function removeUserLanguage(UserLanguage $userLanguage): self
    {
        if ($this->userLanguages->removeElement($userLanguage)) {
            // set the owning side to null (unless already changed)
            if ($userLanguage->getUser() === $this) {
                $userLanguage->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validatePasswordRegularExpresion(ExecutionContextInterface $context, $payload)
    {
        $count = 0;

        if (preg_match('/[a-z]/', $this->getPassword())) {
            $count++;
        }

        if (preg_match('/[A-Z]/', $this->getPassword())) {
            $count++;
        }

        if (preg_match('/[0-9]/', $this->getPassword())) {
            $count++;
        }

        if (preg_match('/[' . preg_quote('~!@#$%^&*()_+[]\;\\\',./{}|:"<>?', '/') . ']/', $this->getPassword())) {
            $count++;
        }

        if ($count < 3) {
            $context->buildViolation('The password must contain at least three sets of characters as described below.')
                ->atPath('password')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function validatePersonalAndLegalFields(ExecutionContextInterface $context, $payload)
    {
        if ($this->getEntityType() === 'private' && $this->getPersonalCode() === null) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('personalCode')
                ->addViolation();
        }

        if ($this->getEntityType() === 'legal') {
            if ($this->getCompanyCode() === null) {
                $context->buildViolation('This value should not be blank.')
                    ->atPath('companyCode')
                    ->addViolation();
            }

            if ($this->getCompanyName() === null) {
                $context->buildViolation('This value should not be blank.')
                    ->atPath('companyName')
                    ->addViolation();
            }
        }
    }
}
