<?php
declare(strict_types=1);
namespace App\Entity;

use App\Repository\UserLanguageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserLanguageRepository::class)
 */
class UserLanguage
{
    public const LANGUAGES = ['english', 'french', 'german', 'lithuanian', 'polish', 'russian'];
    public const PROFICIENCY_LEVELS = ['beginner', 'intermediate', 'fluent'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=UserLanguage::LANGUAGES, message="Choose a valid language.")
     */
    private $language;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=UserLanguage::PROFICIENCY_LEVELS, message="Choose a valid proficiency level.")
     */
    private $proficiency;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userLanguages", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getProficiency(): ?string
    {
        return $this->proficiency;
    }

    public function setProficiency(?string $proficiency): self
    {
        $this->proficiency = $proficiency;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
