<?php
declare(strict_types=1);
namespace App\Form\Type;

use App\Entity\UserLanguage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserLanguageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('language', ChoiceType::class, [
                'label' => '* Language',
                'choices'  => $this->getChoices(UserLanguage::LANGUAGES),
                'placeholder' => 'Choose language'
            ])
            ->add('proficiency', ChoiceType::class, [
                'label' => '* Proficiency',
                'choices'  => $this->getChoices(UserLanguage::PROFICIENCY_LEVELS),
                'placeholder' => 'Choose role'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserLanguage::class,
        ]);
    }

    private function getChoices(array $choices): array
    {
        $array = [];

        foreach ($choices as $v) {
            $array[ucfirst($v)] = $v;
        }

        return $array;
    }
}
