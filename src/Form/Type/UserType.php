<?php
declare(strict_types=1);
namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fullName', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => '* Name Surname']
            ])
            ->add('email', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => '* Email (e.g. name@mail.eu)']
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'first_options'  => [
                    'label' => false,
                    'help' => 'The password should be at least 8 characters long. The password must contain at least three sets of characters: lowercase letters, capital letters, digits, special characters (~!@#$%^&*()_+[]\;\',./{}|:"<>?)',
                    'attr' => ['placeholder' => '* Password']
                ],
                'second_options' => [
                    'label' => false,
                    'attr' => ['placeholder' => '* Repeat Password']
                ],
            ])
            ->add('entityType', ChoiceType::class, [
                'choices'  => [
                    'Private entity' => 'private',
                    'Legal entity' => 'legal',
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => false,
            ])
            ->add('personalCode', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => '* Personal Code']
            ])
            ->add('companyName', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => '* Company Name']
            ])
            ->add('companyCode', TextType::class, [
                'label' => false,
                'attr' => ['placeholder' => '* Company Code']
            ])
            ->add('userLanguages', CollectionType::class, [
                'entry_type' => UserLanguageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'block_name' => 'user_languages',
                'label' => false,
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
