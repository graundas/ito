<?php
declare(strict_types=1);
namespace App\EventSubscriber;

use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class UserEntitySubscriber implements EventSubscriber
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        /**
         * @var $user User
         */
        $user = $args->getObject();

        if (!$user instanceof User) {
            return;
        }

        $email = (new Email())
            ->from('no-replay@hostname') // @TODO: should be taken from ENV parameters.
            ->to($user->getEmail())
            ->subject('User registration')
            ->text("Your registration was successful. Your email is " . $user->getEmail() . " and password is " . $user->getPassword() . ".");

        $this->mailer->send($email);
    }
}
