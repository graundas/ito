import * as $ from "jquery";

let showPersonalFields = function() {
    $("#form_personal_fields").show();
    $("#form_company_fields").hide();
};

let showCompanyFields = function() {
    $("#form_company_fields").show();
    $("#form_personal_fields").hide();
};

let updateFieldsVisibility = function() {
    if ($("#form_entity_type_fields input:checked").val() === 'legal') {
        showCompanyFields();
    } else {
        showPersonalFields();
    }
}

// update on entity type change
$("#form_entity_type_fields input").change(function() {
    updateFieldsVisibility();
})

// update on page load
$(function () {
    updateFieldsVisibility();
})

$('#form_user_language_list').on('click', '.user-language-delete-button', function () {
    $(this).closest('.row').remove();
})

$("#form_add_language").click(function(){
    let list = $($(this).attr('data-list-selector'));

    // Try to find the counter of the list or use the length of the list
    let counter = list.data('widget-counter') || list.children().length;

    // grab the prototype template
    let newWidget = list.attr('data-prototype');

    // replace the "__name__" used in the id and name of the prototype
    // with a number that's unique to languages
    // end name attribute looks like name="user[userLanguages][2][language]"
    newWidget = newWidget.replace(/__name__/g, counter);
    newWidget = $(newWidget);
    // Increase the counter
    counter++;
    // And store it, the length cannot be used if deleting widgets is allowed
    list.data('widget-counter', counter);

    // create a new list element and add it to the list
    let newElem = $(list.attr('data-widget-tags')).html(newWidget);
    newElem.appendTo(list);
})
