<?php
declare(strict_types=1);
namespace App\Tests;

use App\Entity\User;
use App\EventSubscriber\UserEntitySubscriber;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;

class UserEntitySubscriberTest extends TestCase
{
    public function testEmailSendingEvent()
    {
        $user = new User();
        $user
            ->setFullName('Test')
            ->setEmail('test@email.host')
            ->setEntityType('private')
            ->setPersonalCode('65165165');

        $eventStub = $this->getMockBuilder(LifecycleEventArgs::class)->disableOriginalConstructor()->getMock();
        $eventStub->method('getObject')->willReturn($user);

        $mailerStub  = $this->getMockBuilder(MailerInterface::class)->disableOriginalConstructor()->getMock();
        $mailerStub->method('send')->willReturn(null);

        $subscriber = new UserEntitySubscriber($mailerStub);
        $subscriber->postPersist($eventStub);
    }
}
