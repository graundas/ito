## Usage

To compile front-end code use command: 


```bash
npm run dev
```

To run a server use: 

```bash
symfony server:start
```

Admin credentials:

email: admin@hostname

password: qweqwe